import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library
from time import sleep # Import the sleep function from the time module

def clk_ev(pin):
    global old_clk, old_dt
    global rot_change
    sleep(0.001) # important pour éviter des lectures trop rapprochées qui faussent la séquence
    new_clk = GPIO.input(clk)
    new_dt = GPIO.input(dt)

    #print("clk Event ! clk: {}, dt: {}".format(new_clk, new_dt))
    #print("Old ! old_clk: {}, old_dt: {}".format(old_clk, old_dt))

    if new_clk != old_clk or new_dt != old_dt:
        if old_clk == 0 and new_clk == 1:
            rot_change = (old_dt * 2 - 1)
        elif old_dt == 0 and new_dt == 1:
            rot_change = -(old_clk * 2 - 1)

    old_clk, old_dt = new_clk, new_dt

def update_rot_counter():
    global rot_counter, rot_change
    if rot_counter > 1000:
        rot_counter -= 20
    elif rot_counter < 0:
        rot_counter += 20
    elif rot_change != 0:
        rot_counter = rot_counter + 10*rot_change
        rot_change = 0
        print(rot_counter)


def update_led():
    global led_counter, rot_counter, led_state
    if led_counter <= rot_counter/3.0:
        led_counter += 1
    else:
        if led_state:
           GPIO.output(8, GPIO.LOW) # Turn on
           led_state = False
        else:
           GPIO.output(8, GPIO.HIGH) # Turn on
           led_state = True
        led_counter = 0




if __name__ == "__main__":

    led_pin = 8
    clk = 11 #GPIO 17 in BCM mode
    dt = 12 #GPIO 18 in BCM mode

    GPIO.setmode(GPIO.BOARD) # Use physical pin numbering

    # Set led_pin as output with initial value to low (off)
    GPIO.setup(led_pin, GPIO.OUT, initial=GPIO.LOW) 

    # Set both inputs with pull down (the pin goes to off as default value)
    GPIO.setup(clk, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(dt, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    # déclencher une maj de la valeur sur front montant de clk ou dt
    GPIO.add_event_detect(clk, GPIO.RISING, callback=clk_ev)
    GPIO.add_event_detect(dt, GPIO.RISING, callback=clk_ev)

    old_clk = True
    old_dt = True
    rot_change = 0
    rot_counter = 500
    led_counter = 0
    led_state = True


    try:
        while True:
            update_rot_counter()
            update_led()
            sleep(0.001)
            #print("actual_clk:{}, actual_dt:{}".format(GPIO.input(clk), GPIO.input(dt)))
    finally:
            GPIO.cleanup()

